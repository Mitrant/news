<?php
require_once 'application/core/Controller.php';

class Controller_main extends Controller
{
    function __construct(Model_main $model, View_main $view)
    {
        parent::__construct($model, $view);
    }


    function action_index()
    {
        $news = $this->model->get_all_news();

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {

            $this->view->view_index($news);
        } else {
            $id = $this->model()->rec_vote();
            $this->view->view_index($news, $id);
        }

    }
}
