<?php

class Route
{
    function Start()
    {
        $name = 'main';
        $action = 'index';
        $route = $_SERVER['REQUEST_URI'];

        $route = explode('/', $route);

        if (!empty($route[1])) {
            $name = $route[1];
        }
        if (!empty($route[2])) {
            $action = $route[2];
        }

        $model_name = 'Model_' . $name;
        $model_path = 'application/models/' . $model_name . '.php';

        $view_name = 'View_' . $name;
        $view_path = 'application/views/' . $view_name . '.php';

        $controller_name = 'Controller_' . $name;
        $action_name = 'action_' . $action;
        $controller_path = 'application/controllers/' . $controller_name . '.php';


        if (file_exists($controller_path) && file_exists($model_path) && file_exists($view_path)) {

            require_once $controller_path;
            require_once $model_path;
            require_once $view_path;

            $view = new $view_name();
            $model = new $model_name();
            $controller = new $controller_name($model, $view);

            if (method_exists($controller, $action_name)) {

                $controller->$action_name();

            } else {

                self::error404();
            }
        } else {
            self::error404();
        }
    }

    function error404()
    {
        echo '404';
    }
}
