<?php

class Controller 
{
    protected $model;
    protected $view;
    
    function __construct(Model $model, View $view)
    {

        $this->model = $model;
        $this->view = $view;
    }
    
    function model()
    {
        return $this->model;
    }
    
    function view()
    {
        return $this->view;
    }
}