<?php

class Model {

    private $connect;

    function __construct()
    {

        $sql = 'mysql';
        $host = 'localhost';
        $db_name = 'news';
        $connect = $sql . ':' . 'host=' . $host . ';' . 'dbname=' . $db_name;
        $user = 'root';
        $pass = '';

        $this->connect = new PDO($connect, $user, $pass);
        $this->connect->query('SET NAMES utf8');
    }

    function connect_db() 
    {
        return $this->connect;
    }
}
